import { PerkFull } from 'global'

export const alternativeTheming = `
<h3 class="text-lg font-semibold text-center">Alternative Symbiote Theming</h3>
<p>Xenomorphs aren’t for everyone, and that’s okay. If your chosen queen is canonically associated with metaphysical corruption or purification, or are canonically known for polymorphing others or spawning or controlling large numbers of mooks, all stages of the symbiote life cycle will follow their aesthetic and theming instead in all parts of the symbiote ecosystem. The functionality of each part will remain the same. Such queens include (but are not limited to) Salem, Madokami, most versions of Circe, and the subjects of certain waifu perks. This is a canonicity factor and is thus affected by Power Swap.</p>
`

export const symbioteQueen = `
<h3 class="text-lg font-semibold text-center">Symbiote: The Swarm Queen</h3>
<p>One single waifu in your initial purchase will be the queen to your king. You and she both will already be merged with your symbiotes when you arrive in your first world, and gain an additional +1 tier, capped at 10. If the queen is already T10, her power will double, but her effective tier will not rise any further. Symbiosis alters a subject’s mind, making them see you as a mate (Companions), king (Familiars), or both, whose orders are to be obeyed and whose survival is paramount to the species. All symbiote hosts besides yourself will see the queen as the first among them, regardless of previous relationships, and arrange themselves into a pyramidal hierarchy under her. If you do not already have a queen, the first female subject that you capture using any Symbiote-related method will automatically become your queen.</p>
<p>The queen must always be female. If you are naturally female or Possessed or Substituted a female vessel, you may act as your own queen, with no need for a king - only consorts. A lone female in an otherwise all-male swarm will always be the queen, while a primarily male swarm still whitelist at least one female Familiar for the role. If your queen is part of a set that can only be purchased together, the actual queen will be the most dominant one according to their natural personalities; if the members of the set are truly co-equal, then they’ll serve as co-equal queens.</p>
<p>The King and Queen are visibly distinct from the rest of a swarm. Their initial blending with their symbiote is accompanied by non-optional physical growth, affecting the peak of their physical maturity. Swarm royals who are not yet physically mature will grow into this state naturally over time. Both king and queen experience significant increases in muscle definition, particularly in the arms, abdominals, glutes, and legs. The queen also experiences severe non- muscular growth of the breasts, rear and thighs, while the king instead gains even more muscle and increased genital size, even beyond any improvements from Body Tune-Up.</p>
<p>Regardless of gender, the target sizes for any trait are roughly 130% on the scales defined in Advanced Tempest Runes (Body), with up to 10% error in either direction. If desired, this growth may recede down to the royals’ baseline sizes at a rate of 3% every 5 hours. The full extent of this recession, if any, depends on the contractor’s desires. This growth may be manually re-triggered, with the aid of a larva or swarm member, at any time.</p>
<p>You and your queen may always capture a target in person by transmitting a transformative fluid from your mouth or tail. You or your queen can let your tail out without the rest of the armor, make out or have sex with the conversion target, and then leave. This transformation takes about two hours to complete; the target will experience a gradually-building surge of lust during that time, which will only fade when satisfied by you or another symbiote host whose lust surge has previously been satisfied. Capture credits are rewarded once this lust has faded. <strong>Targets that are not sexually mature cannot be captured in this way.</strong></p>
`

export const symbioteUnits = `
<h3 class="text-lg font-semibold text-center">Symbiote: Units</h3>
<p>One of the four larva types is granted for free with the basic symbiote package, as the Binding is only barely usable as a binding without one. You can choose a second larva type once you’ve built a Lair, a third once you’ve built a Colony, and the last once you’ve built a Hive.</p>
<p>Regardless of your chosen larva type, you will always begin with two full clutches of mature specimens, ready to bind your starting companions and familiars, though the number of individuals per clutch varies wildly. In addition to their basic effects, the default binding tattoos will prevent your subjects from lethally harming any of your symbiote units, and they will usually accept the conversion outright if they know you want them to.</p>
<p>Additional eggs, and thus larvae, come from your queen. First, you must fertilize your queen in the traditional human fashion. If you have Fertility Calibration, you may choose between egg production (for each type of larva), live humanoid births (see the Offspring section under Additional Rules) and contraception. Results are otherwise random. A symbiote host’s tail may act as a phallus substitute for this purpose, but queens cannot self-impregnate. Fertility Calibration’s normal effects will only apply to live humanoid births.</p>
<p>Either way, the queen will produce a clutch of eggs over the next four weeks. All symbiote eggs will hatch after two weeks of incubation in ideal (warm) conditions, but can take up to four weeks if too hot or too cold.</p>
<p>Other units are produced at the buildings that unlock them. Each of their clutches only has a single egg, which whitelist far more time to hatch and mature to a combat- ready state. With an Evolution Chamber, your geneticists and bioengineers can customize these defaults to suit your force’s needs. The growth acceleration functions of Nurseries and Groves apply to all non-humanoid units, whether larvae or larger.</p>
`

export const symbioteStructures = `
<h3 class="text-lg font-semibold text-center">Symbiote: Structures</h3>
<p>The Alterzelu symbiote is part of a four-stage ecosystem, including a variety of units and buildings. Credit payments are required to access each tier of buildings, but not for individual buildings. Instead, the cost of each building comes from the bodies of your non-queen, sapient, subjects that the building is grown from. Devotion Cost is calculated identically here as it is for Dungeons. Having at least a Lair allows certain larvae types to be used for Devotion as well: 4 Skitterers and 8 Globsters are each worth 1 Devotion point.</p>
<p>This construction process begins when the designated subjects huddle together as their symbiote bio-armors manifest, flow into one another, and grow to encompass the group. The individuals within will lose consciousness and become one with the new structure. If the structure is destroyed, they die as well. Artistic effigies of the component subjects will remain visible somewhere on or inside the structure, whether a mural, carving, statue, or other feature. Defacing or destroying an effigy will do nothing to free the depicted subject. Structure descriptions state the default dimensions, but all can be grown as any contiguous shape, not exceeding the defined space.</p>
<p>If you have Conjunction, you can choose for the Devoted subjects to no longer be unconscious, but instead form a single mind that represents the will of the building. This mind can communicate its status and (for production buildings) follow lengthy build orders with complex conditions.</p>
<p>Humanoid subjects Devoted to a swarm structure cannot be disconnected from it casually, but disconnection is still possible. Replacement components must be provided in advance, however, or else the structure will dismantle itself completely. A 24-hour transition period will see the new component swapped in and the old component emerge, free to move once more. Devotion is permanent for Larvae; they will die if removed.</p>
<p>Individual subjects with high enough Devotion values may be used to construct multiple buildings at once. All such buildings grown out of single individuals will be grown together as a single contiguous mass and can only be dismantled together as well.</p>
`

export const structureUpgrades = `
<h3 class="text-lg font-semibold text-center">Structure Upgrades</h3>
<p>Many Symbiote buildings, including the Hatchery, have upgraded forms. During the upgrade process, the structure will grow to encompass a much larger area, pausing production at the site for the duration. Symbiote structures caught within an upgrade’s footprint may remain standing, with the structure growing around them, or be dismantled and any subjects used for their construction variously released or absorbed into the upgrade instead. This reduces the cost of the upgrade by their Devotion value.</p>
`

export const expansions = `
<h3 class="text-lg font-semibold text-center">Symbiote: Expansions</h3>
<p>Expansion perks for the Symbiote are paid for with regular credits and often include a symbiote structure as a prerequisite. If you only have an upgraded form of the listed prerequisite structure, it still counts.</p>
<p>If you buy an Expansion that whitelist a structure, and all examples of the structure are dismantled or destroyed, the Expansion’s effects will persist until you take a refund for the Expansion. At that point, repurchasing the Expansion will require building a new version of the appropriate structure.</p>
<p>If an Expansion applies a specific type of structure (e.g. Hatchery), all upgraded versions of that structure (e.g. Lair, Colony, and Hive) will also benefit.</p>
`

export const creep = `
<h3 class="text-lg font-semibold text-center">Creep</h3>
<p>Hatcheries and Fortresses will slowly infect the stationary solid matter in their environment with an organic substrate called creep. Creep is not a solid mass; rooms and corridors will naturally form inside it and will not be blocked off by further expansion. Creep will not assimilate your retinue’s things, nor will it assimilate sapient beings (even inorganics) or non-sapient animals - at least, not with only a basic Hatchery. Plants and fungi are not so lucky. If a Creep-generating structure is dismantled, the creep it supports will recede over the course of 1 week (168 hours).</p>
<p>Creep provides an alternative food source for symbiote creatures of all sorts, including your retinue. Creep provides all necessary nutrients for creatures who feed on it to reach maturity. Any food sources assimilated into your creep zone will continue to grow naturally. These foods will become especially tasty and nutritious for your retinue members, with no added effects on outsiders - at least, not with only a basic Hatchery.</p>
<p>When the Creep zones of two separate swarms meet, the precise border between the two depends on the strength of the local sources of Creep. If the strongest sources on each side of a border section are equal, the border will be halfway between the two; if one is stronger than the other, the stronger source will push the border closer to the weaker one in proportion to their strengths, up to a core radius around the building itself: 25% of the building’s baseline Creep spread. This is usually combined with a stronger push around the weaker source’s flanks. Border push will continue to occur elsewhere around the weaker source as the gap between the two increases, until the weaker source is completely encircled and cut off from the rest of its faction’s Creep zone. This leaves all freestanding structures in the overrun area vulnerable to the overrunner’s whims.</p>
`

interface Units {
  uid: string
  title: string
  type: string
  image: string
  desc: string
  whitelist?: string[]
}

export const synUnits: Units[] = [
  {
    uid: 'KYeJi',
    title: 'Buzzers',
    type: 'Larva',
    cost: 0,
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1032739343195983963/Screenshot_3.jpg',
    desc: `
    <p>These wasp-like creatures, as large as an adult human’s fist, can fly to a local altitude of 100m. Buzzers’ mandibles and stingers are laced with transformative fluid, allowing them to infect a target through any open wound or fluid. This dose is quite small, and thus whitelist far more time to take effect than those of other larva types. One Buzzer carries enough transformative to dose 12 targets before it falls to the ground, dead.</p>
    <p>One clutch of buzzers has 24 eggs, with each one roughly the size of a quail’s. Buzzers typically feed on fruits, but can cultivate their own type of honey as a more stable food supply. They require three days to feed and grow large enough for practical use.</p>
    <p>A Buzzer’s fluid takes two weeks (336 hours) to thoroughly transform a target on the inside, with no external side-effects. After that process completes, the target will transform into a member of your swarm upon skin contact with another member of your swarm. Only then will the capture be confirmed and the appropriate number of credits awarded.</p>
    `,
  },
  {
    uid: 'wQZU5',
    title: 'Floaters',
    type: 'Larva',
    cost: 0,
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1032740233621536880/Screenshot_5.jpg',
    desc: `
    <p>These jellyfish-like creatures, with main bodies large enough to hold a human within, move as their name suggests and can reach anywhere in a local atmosphere. Their tentacles carry a chemical sedative that most creatures absorb on skin contact and can knock out a tier 2 adult human within twenty seconds. Resistance to this chemical is increasingly common in higher-tiered targets: at effective tier (after step 6) 6 or above, most targets are too resistant, or can fight back too well, for Floater-based capturing to remain viable. They can escape a Floater’s grasp (or even kill it outright!) before the sedative can fully affect them.</p>
    <p>Floater clutches have only four eggs each, but the egg is very large, roughly the size of an ostrich’s. Floaters are obligate carnivores and prefer large game. They require two weeks to feed and grow large enough for practical use, but can continue growing beyond that point, with no hard limit. Multiple Floaters can join together to abduct especially large or numerous targets. When this occurs, they cannot separate until the capture completes.</p>
    <p>Floaters approach their targets from the air, but do not directly convert their targets on-site. Instead, their tentacles reach down to grab and sedate the target, then reel them into the Floater’s main body. Floaters can abduct as many targets as their bodies can hold. Once full, the Floater returns to the nearest Hatchery-line structure and attaches itself to the structure’s exterior surface. At that point, the Floater hardens into an opaque cocoon before finally transforming and converting the target into a member of your swarm. If you lack any Hatcheries, a tree or pond will also work, doubling the base conversion time.</p>
    <p>Conversion takes 48 hours after the cocoon hardens. The target is already in physical contact with part of your swarm, and thus does not need additional confirmation to count as a capture. The Hatchery’s exterior, and the side of the cocoon that touches it, will open for the new subject, allowing them to slide or crawl into the nearest open space within the Hatchery and then walk outside on their own. The Floater remains intact, so it can be reused.</p>
    <p>Breaking the cocoon early will halt the target’s conversion. The process may be continued if the target is recaptured at any time before they exit your creep’s zone, at which point all progress is lost.</p>
    `,
  },
  {
    uid: 'qaVE7',
    title: 'Globsters',
    type: 'Larva',
    cost: 0,
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038867524902723725/zyro-image.png',
    desc: `
    <p>These slime creatures, roughly the size of a watermelon, can spread themselves thin to flow across solid surfaces undetected, including walls and ceilings. Together with a limited form of camouflage, to mimic the appearance of a single surface that they have contact with, they have the most functional stealth of all larva types, though they can still be detected by supernatural means. Globsters cannot be fought off by any target of effective tier (after step 6) 4 or below.</p>
    <p>One clutch of Globsters has 16 eggs, with each one roughly the size of a chicken’s. Their eggs don’t so much hatch as melt into the newly-born Globster when the time is right. Globsters are omnivorous and will devour whatever is small enough for them to engulf. They require one week to feed and grow large enough for practical use.</p>
    <p>When a Globster strikes, it flows over the target’s body until it makes skin contact. Any part of the skin will do. The Globster then melts into the target’s body, merging with it in a non-sexual manner to directly transform the target over the course of about five minutes.</p>
    <p>After the transformation completes, the new subject will experience a mental haze that compels them to seek either the nearest existing sapient swarm member or Hatchery-line structure, whichever is closer. It will not fade until they succeed and make skin contact. Only then will the capture be confirmed and the appropriate number of credits awarded.</p>
    `,
  },
  {
    uid: '8Q4UU',
    title: 'Skitterers',
    type: 'Larva',
    cost: 0,
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1032758849909833790/828456-Alien-Monsters-Painting-Art-Teeth-Fan-ART.jpg',
    desc: `
    <p>The original Alterzelu larva type, these long-tailed pseudo-arachnids generally resemble a small Xenomorph, but always have the strongest similarity to a symbiote host’s bioarmor, no matter the queen’s theme. Skitterers are comparable in size to a small dog or wild cat, such as a beagle or lynx. They can climb up walls and across ceilings with ease and use their tails to reach surprising speeds underwater. Though not truly sapient, living only to merge with a future host, Skitterers are dangerously clever, capable of using teamwork, tactics, and trickery. They cannot be fought off by any target of effective tier (after step 6) 5 or below. Outside of a theoretical “white room” scenario, they can leverage their advantages to pose major threats against even some Tier 7s and 8s.</p>
    <p>One clutch of Skitterers has 8 eggs, with each one roughly the size of a goose’s. Skitterers are obligate carnivores, preferring small mammals and fish, and require one week to feed and grow large enough for practical use.</p>
    <p>When a Skitterer strikes, it will latch onto its target - typically the head or torso - and find at least one orifice to violate with its tongue or tail. As it pumps its transformative fluid into the target, it will attempt to align its spine with the target’s and engulf the target’s head. The skitterer will also clamp its legs around the target, piercing their skin and allowing the entire creature to liquify itself and flow into the target to become the new host’s bioarmor.</p>
    <p>The full transformation process usually takes no more than five minutes. As the transformation completes, the new subject’s libido will skyrocket, sending them into a haze that will not fade until they have sex with an existing sapient swarm member. <strong>Skitterers cannot capture sexually immature targets.</strong> The capture will be confirmed, and the appropriate number of credits awarded, only after this sex occurs.</p>
    `,
  },
  {
    uid: 'tIgHr',
    title: 'Lingoth',
    whitelist: ['Burrow'],
    type: 'Tank',
    cost: 0,
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1039522417908920320/sample-3f625fad3a9527b3f238bb147df2ed8b.jpg',
    desc: `
    <p>A Lingoth is a modified ankylosaurus, a muscular, thick-skinned quadruped with a heavily-armored spiked shell and long, clubbed tail. Lingoths are vegetarian, focused on low- growing shrubs and fruits, and will make great use of any Groves within your creep zone.</p>
    <p>With Maru Mari, Lingoths will grow a round hatch where humanoid swarm members can access it, allowing them entrance to an internal passenger space while in ball form. Junior Lingoths may carry a single passenger in this way, Adult Lingoths 20, and Elder Lingoths 400. While inside a Lingoth, swarm members must remain in ball form, but can remain aware of the outside world by tapping into their ride’s senses. With Presence, they can project ghostly avatars anywhere within 2 m of the Lingoth’s shell and use their own senses instead.</p>
    <p>A newly-hatched Lingoth will become combat- ready, as a Junior Lingoth, about six months (180 days) after hatching. Junior Lingoths measure about 6-8 m long and 1 m tall at the shoulder, with an overall height of about 2 m. Bony spikes on their head and neck make them a threat in melee range. Chemical sacs in their throats allow them to spit and ignite a mixture very similar to napalm. This “breath” weapon has an effective range of about 50 m with a focused stream, or 40 m with a wide spray. Careful use of this weapon is advised: the sacs can only hold up to five minutes of fuel, and once emptied require 1 week to refill, assuming the creature maintains a normal appetite.</p>
    <p>With a Colony, Junior Lingoths will continue to grow and become Adult Lingoths at about 2 years of age. Adult Lingoths measure about 18-24 m long and 3 m tall at the shoulder, with an overall height of about 6 m. They are even beefier, spikier, and more armored than their younger selves, but the main threat is now ranged. The chemical sacs they used before no longer hold conventional incendiaries but a superheated plasma fluid that can be ejected at relativistic speeds toward any target in the Lingoth’s line of sight. This weapon can only be active for up to 30 seconds before the Lingoth needs to stop and let the flesh in its mouth literally cool down for at least 3 minutes.</p>
    <p>With a Hive, Adult Lingoths will continue to grow and become Elder Lingoths at about 10 years of age. Elder Lingoths measure about 54-72 m long and 9 m tall at the shoulder, with an overall height of about 18 m. They will barely notice most conventional melee threats, and their mouths can, finally, fully endure the stresses of plasma breath use. Fuel becomes the limiting factor once more: though an Elder Lingoth’s plasma sacs can hold up to 12 hours (720 minutes) of fuel, emptied sacs will only refill after 4 weeks, assuming the creature maintains a normal appetite.</p>
    `,
  },
  {
    uid: 'Odhry',
    title: 'Linkor',
    whitelist: ['Burrow'],
    type: 'Artillery',
    cost: 0,
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038869452466434129/3522-wasteland-scorpion.jpg',
    desc: `
    <p>The Linkor is a modified giant scorpion, with the stinger replaced with a quill launcher for indirect fire. Linkors are pack-hunting carnivores: one grapples a prey and the rest shoot their quills at the prey until it dies. Linkor exoskeletons are relatively light, offering much less protection than a Lingoth shell and no interior space for Maru Mari or cargo. Any would-be passengers must ride on a Linkor’s back.</p>
    <p>The standard quill, used by all Linkors, measures 140 mm across and 35 cm long, and takes 30 seconds to grow within the launcher. Chemicals inside the launch tube keep quills malleable so the Linkor can move its tail properly. After launch, with those chemicals no longer present, quills rapidly harden into a more recognizable 105 cm spike, with small fins for more stable flight.</p>
    <p>A newly-hatched Linkor will become combat-ready as a Junior Linkor, about six months (180 days) after hatching. Junior Linkors measure about 3-4 m long to the base of the tail and 1 m tall; the tail adds another 3-4 m in length. Junior Linkors’ tails are only large enough to shoot a single quill at a time, can hold up to 3 quills in reserve. Junior Linkors have an effective firing range of about 4 km.</p>
    <p>With a Colony, Junior Linkors will continue to grow and become Adult Linkors at about 2 years of age. Adult Linkors measure about 9-12 m long to the base of the tail and 3 m tall; the tail adds another 9-12 m in length. Adult Linkors can continue to shoot solid quills like their younger selves. They may also shoot a second quill type, this one hollow and filled with a binary liquid explosive. When the quill hits its target, the shock of impact causes the two parts to mix and detonate. An Adult Linkor’s thicker tail allows it to effectively handle its 6 individual launchers, each of which can hold up to 18 quills in reserve. Adult Linkors have an effective firing range of about 12 km.</p>
    <p>With a Hive, Adult Linkors will continue to grow and become Elder Linkors at about 10 years of age. Elder Linkors measure about 27-36 m long to the base of the tail and 9 m tall; the tail adds another 27-36 m in length. Elder Linkor quills, whether solid or explosive, contain small, sub- sapient nervous systems that allow a quill to track a moving target on its own and adjust its course with psionic power. An Elder Linkor’s tail has 36 individual launchers, each of which can hold up to 54 quills in reserve. Elder Linkors have an effective range of 36 km.</p>
    <p>With Conjunction, all Linkors can use nearby swarms of Buzzers as spotters, to more effectively aim at targets beyond their lines of sight. This extends a Junior Linkor’s effective range to 14 km, an Adult Linkor’s effective range to 84 km, and an Elder Linkor’s effective range to 378 km.</p>
    `,
  },
  {
    uid: 'X6qsR',
    title: 'Linguu',
    type: 'Support',
    cost: 0,
    whitelist: ['Burrow'],
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038870047797555342/969694.jpg',
    desc: `
    <p>The Linguu is a close variant of the Lingoth, but with a taller dome, closer to a tortoise’s, and a short neck and tail. They share the same diet as their Lingoth cousins. They have no inherent offensive abilities, beyond simply being large creatures that can ram or trample smaller creatures that get in their way. They make up for that in defense, with a significantly tougher shell than a Lingoth’s.</p>
    <p>A Linguu shell is hollow at all stages. Linguu bellies have a loading ramp that can drop down to the ground below, allowing humanoids or cargo to enter on foot. Specialized ligaments between the main cabin and the outer part of the shell act as a suspension system, ensuring a smooth ride. With Maru Mari, this space between the outer shell and the cabin wall will be accessible to ball forms, allowing them to remain secure while the individuals sleep, project their Presence outside (within 2 m of the Linguu’s shell), or enter the local Mindscape.</p>
    <p>A newly-hatched Linguu will become large enough to carry useful loads, as a Junior Linguu, about five months (150 days) after hatching. Junior Linguus measure about 9-12 m long and 2 m tall at the shoulder, with an overall height of about 8 m. A Junior Linguu’s main cabin offers standing room for up to 40 humanoids, while up to 200 ball forms can enter the Maru Mari storage area.</p>
    <p>With a Colony, Junior Lingus will continue to grow and become Adult Linguus at about 19 months (570 days) of age. Adult Linguus measure about 27-36 m long and 6 m tall at the shoulder, with an overall height of about 24 m. An Adult Linguu’s main cabin offers standing room for up to 600 humanoids, while up to 1,800 ball forms can enter the Maru Mari storage area. Additionally, Adult Linguus can see ten seconds into the future. Using that ability together with the spikes on their shells, they can deflect incoming projectiles or detonate them prematurely to minimize any harm.</p>
    <p>With a Hive, Adult Linguus will continue to grow and become Elder Lingus at about 8 years of age. Elder Linguus measure about 81-108 m long and 18 m tall at the shoulder, with an overall height of about 72 m. An Elder Linguu’s main cabin offers standing room for up to 15,000 humanoids, while up to 16,200 ball forms can enter the Maru Mari storage area. Elder Linguus’ future sight reaches up to a minute into the future, and their shell is nearly impenetrable to conventional attack.</p>
    `,
  },
  {
    uid: 'J3Rbv',
    title: 'Ailgoth',
    type: 'Air Superiority',
    cost: 0,
    whitelist: ['Nest'],
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1039514270926381186/unknown.png',
    desc: `
    <p>The Ailgoth is a modified eagle, with additional traits from bats and pterosaurs. Ailgoths hunt in pairs over land and at sea. The talons on both their feet and wings allow them to carry heavy loads back to their nests, though especially large prey must still be eaten on site. These are not necessarily mated pairs; an Ailgoth is as likely to hunt with a humanoid rider as another Ailgoth.</p>
    <p>An Ailgoth’s airspeed is greatly limited when carrying anything in its talons. When one is not loaded down, it can tuck its feet and wings into its body, cutting its apparent wingspan by two-thirds and allowing it to reach its full speed. Conversely, untucked Ailgoth wings are a highly effective airbrake.</p>
    <p>While its sharp claws and beak are suitable for melee, an Ailgoth’s primary ranged weapon is a wing- mounted quill launcher. These launchers operate on the same principles as their Linkor counterparts. Ailgoth quills are far smaller, at 20 mm wide and 10 cm long, but 20 quills can be launched every second and 6 replacements regrown in that same time. Older Ailgoths can launch Linkor quills as well. The exact number of quill launchers and their ammo capacities are both dependent on an Ailgoth’s life stage.</p>
    <p>A newly-hatched Ailgoth will become combat-ready, as a Junior Ailgoth, about eight months (240 days) after hatching. A Junior Ailgoth’s main body measures about 2-2.5 m long and 1 m wide, and its wingspan is about 4-5 m across. It can release a sonic burst to disrupt its targets’ balance, knocking the enemy or prey to the ground or a water’s surface and leaving them prone to the Ailgoth’s other natural weapons. It has a single 20mm quill launcher in each wing, can hold up to 400 quills in reserve for each launcher, and reach a maximum airspeed of 600 km/h.</p>
    <p>With a Colony, Junior Ailgoths will continue to grow and become Adult Ailgoths at about 32 months (960 days) of age. An Adult Ailgoth’s main body measures about 6-7.5 m long and 2 m wide, and its wingspan is about 12-15 m across. Adult Ailgoth wings sport 4 quill launchers each, with up to 1,000 quills in reserve for each launcher, as well as a single large (Linkor) quill launcher on each side, with up to 3 large quills in reserve. Both large and small quills can be filled with the same binary liquid explosive that Adult Linkors use. Adult Ailgoths can reach a maximum airspeed of 1,500 km/h (mach 1.2).</p>
    <p>With a Hive, Adult Ailgoths will continue to grow and become Elder Ailgoths at about 13 years of age. An Elder Ailgoth’s main body measures about 18-22.5 m long and 4 m wide, and its wingspan is about 36-45 m across. Elder Ailgoth wings sport 8 small quill launchers, with up to 2,500 quills in reserve for each launcher, and 4 large quill launchers each, with up to 18 quills in reserve for each launcher. The large quills have the same sub-sapient nervous systems that Elder Linkors use. Elder Ailgoths can reach a maximum airspeed of 3,750 km/h (mach 3.0).</p>
    `,
  },
  {
    uid: 'MJuFy',
    title: 'Ailkor',
    whitelist: ['Nest'],
    cost: 0,
    type: 'Ground Attack',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038871864254140436/tumblr_pl90wjNNIX1v972zjo1_1280.jpg',
    desc: `
    <p>The Ailkor is a modified giant moth that feeds exclusively on Buzzer honey. Ailkors can release chemical powders on command, from their four wings, that can inflict a variety of different effects on whatever living things are unfortunate enough to be caught in such clouds. The older and larger the Ailkor, the wider the cloud it can produce on its own.</p>
    <p>A newly-hatched Ailkor will become combat-ready, as a Junior Ailkor, about 12 months after hatching. A Junior Ailkor’s main body measures about 5-6 m long and 2-2.5 m wide, and its wingspan is about 7-8 m across. They can only release a single type of powder, a mist that dampens sound and reduces local visibility to less than 10 m.</p>
    <p>With a Colony, Junior Ailkors will continue to grow and become Adult Ailkors at about 4 years of age. An Adult Ailkor’s main body measures about 15-18 m long and 6-7.5 m wide, and its wingspan is about 21-24 m across. Adult Ailkors can selectively release any of three different powder types, including the kind they could release as Juniors. The second powder jams most wireless communications and disables most sapient species’ sense of direction. This effectively shuts down a hostile force’s ability to coordinate and navigate within the affected area. The third powder type is a fuel-air explosive. An organ at the end of their tail will ignite the thermobaric cloud, with just enough distance that the Ailkor isn’t harmed.</p>
    <p>With a Hive, Adult Ailkors will continue to grow and become Elder Ailkors at about 20 years of age. An Elder Ailkor’s main body measures about 45-54 m long and 18-22.5 m wide, and its wingspan is about 63-72 m across. Elder Ailkors can release both disabling powders at once as a single cloud. Their explosive powder is now made of microscopic plasma balls, which function just as well in vacuum environments as in an oxygen-rich atmosphere.</p>
    `,
  },
]

export const symbioteBinding: PerkFull[] = [
  {
    uid: 'grbul',
    title: 'Alterzelu Symbiote',
    cost: 125,
    category: 'Binding',
    type: 'Symbiote',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1043498457077383168/ss.png',
    complex: 'target',
    freebies: {
      talentPerks: [{ title: 'Everlasting Talent', cost: 0, count: 1 }, { title: 'Body Tune-Up', cost: 0, count: 1 }, { title: 'Athletic Talent', cost: 0, count: 1 }, { title: 'Martial Talent', cost: 0, count: 1 }, { title: 'Wild Talent', cost: 0, count: 1 }, { title: 'Talent Sharing', cost: 0, complex: [{ flavor: 'Martial Talent', cost: 0 }], count: 1 }, { title: 'Talent Sharing', cost: 0, complex: [{ flavor: 'Wild Talent', cost: 0 }], count: 1 }, { title: 'Talent Sharing', cost: 0, complex: [{ flavor: 'Athletic Talent', cost: 0 }], count: 1 }],
      defensePerks: [{ title: 'Body Defense', cost: 0, count: 1 }, { title: 'Wild Defense', cost: 0, count: 1 }, { title: 'Environmental Defense', cost: 0, count: 1 }],
    },
    desc: `
    <p>Build your own swarm using this engineered species of alien symbiotes. You and all bound retinue members benefit from Body Tune-Up and Everlasting, Athletic, Martial, and Wild Talents. (Take these four perks, plus Talent Sharing (Martial), Talent Sharing (Wild), and Talent Sharing (Athletic), for free.) Take one copy each of Body, Wild, and Environmental Defenses for free as well.</p>
    <p>Alterzelu hosts can, at will, form an environmentally- sealed suit of bio-armor, with Gigeresque black chitin aesthetics, a retractable helmet, sharp claws, and a long tail for combat and maneuverability. This includes you. Existing enchanted gear or otherwise super outfits, such as Life Fibers, power armor, or magical girl uniforms, will be integrated into this bio-armor. This does not apply to gear that is held rather than worn. Hosts also gain superhuman senses, strength, agility, and durability; this is enough to push any blue-star subject up to Tier 4 or any copper-star up one tier, but is not a significant gain for higher tiers.</p>
    `,
  },
  {
    uid: 'muNec',
    title: 'Transfer Spawn',
    cost: 10,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040739968261304320/Screenshot_10.jpg',
    desc: `
    <p>The sympathetic connection between the queen and the rest of the hive allows her to transfer her new eggs directly to the nearest Hatchery that has room to spare in the same creep zone. This ability will fail if all Hatcheries within that zone are already full.</p>
    `,
    whitelist: ['Hatchery'],
  },
  {
    uid: 'qH4ky',
    title: 'Corruptive Ooze',
    cost: 20,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040722352947863602/Screenshot_8.jpg',
    desc: `
    <p>The fluid in a Lair’s spawning pool is highly viscous and mutative. Anyone who touches the surface, who is not already part of your swarm, will be pulled under, then transformed, converted, and captured into a new member of your swarm within a minute. Wyldscape Defense protects against the transformation, though Environmental Defense is also recommended for anyone attempting to crawl out the hard way.</p>
    `,
    whitelist: ['Lair'],
  },
  {
    uid: 'lsAnq',
    title: 'Maru Mari',
    cost: 15,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://64.media.tumblr.com/193ba9f95921b165eb8f7bc73c9dc357/0f6261c49c8e6c15-19/s1280x1920/6ef505046cbd4d4c74fadaf9c63d9c88e8ed00dd.jpg',
    desc: `
    <p>Sapient, human-sized swarm members may now curl themselves into a spherical ball, roughly 1m in diameter. This ball form has no distinct internal bits and a fully-armored exterior. Swarm members may roll around freely in ball form, but often have trouble on slopes.</p>
    <p>Active Burrows, Nests, and Reefs now have a second entrance to the spawning organ, appropriately sized for the ball form to roll into. This entrance leads into the core of the spawning organ, where a swarm member in ball form will be reconfigured into a major unit’s egg and eventually be reborn as the new unit’s animate intelligence.</p>
    <p>If you have Presence or Mindscape, the animate intelligence may continue to interact with others while their body is reconfigured, whether in person at the production site or within the mindscape. Animate intelligences can regain their humanoid forms through Bifurcation, as a side-effect of Warranty Plan, or by entering the spawning pool at any production building to shed the excess biomass and wade back out in humanoid form.</p>
    `,
    needed: 2,
    whitelist: ['Evolution Chamber', 'Burrow', 'Nest', 'Reef'],
  },
  {
    uid: 'kvCJq',
    title: 'Collective Burden',
    cost: 15,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1041030310265094144/22.png',
    desc: `
    <p>Many hands make light work. Your Lairs now have multiple spawning organs, allowing more than one individual to contribute new clutches to the building’s shared spawning pool at the same time. Each Lair now has 3 spawning organs, each Colony has 9, and each Hive has 27. The new spawning organs function identically to the original. Burrows, Nests, and Reefs do not benefit from this perk.</p>
    `,
    whitelist: ['Colony'],
  },
  {
    uid: 'YCljA',
    title: 'Presence',
    cost: 15,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040738856300970066/Screenshot_9.jpg',
    desc: `
    <p>Individuals installed into a Lair, Burrow, Nest, or Reef can now project a psychic avatar anywhere in the structure, allowing them more options to mingle with other members of your swarm. This does not help them ignore the sensations that their flesh body experiences. Individuals who have not yet cultivated that ability tend to exhibit erratic behavior - similar to, as our beta testers frequently reported, “clubgoers on ecstasy.”</p>
    `,
    whitelist: ['Nexus'],
  },
  {
    uid: 'slREP',
    title: 'Mindscape',
    cost: 30,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://images.fineartamerica.com/images/artworkimages/mediumlarge/3/deviant-illusions-of-the-hivemind-susan-maxwell-schmidt.jpg',
    desc: `
    <p>You, your Queens, and all individuals installed into a Lair, Burrow, Nest, or Reef can now project their minds into a virtual space within the swarm’s Conjunction network, allowing for face-to-face communication and activities. Lucid dreaming rules apply: if someone in this space can imagine it, they can manifest it. However, this space cannot accurately represent scenes or places that the swarm has never observed, whether directly or via existing supernatural abilities. Separate mindscapes are also available to Maru Mari individuals riding inside Linguus and other passenger- carrying units.</p>
    `,
    whitelist: ['Hive', 'Presence', 'Conjunction'],
  },
  {
    uid: 'R0qt6',
    title: 'Panspermia',
    cost: 50,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://i.pinimg.com/originals/ce/3f/ae/ce3fae62ab9bdb109e8f63329d616bf2.png',
    desc: `
    <p>You may now build mobile Hatcheries, Burrows, Nests, Reefs, Evolution Chambers, and Nexuses into air, sea, or space units that are large enough to hold them. These may be used to resupply forces in transit or act as seeds to colonize distant lands.</p>
    `,
    needed: 2,
    whitelist: ['Colony', 'Nest', 'Reef'],
  },
  {
    uid: 'wYFvx',
    title: 'Psi Gate',
    cost: 20,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdna.artstation.com/p/assets/images/images/002/421/644/large/piero-macgowan-xcom2-25-psi-gate.jpg?1461551889',
    desc: `
    <p>Your Colonies and Fortresses now include portals that enable instant travel from one to another. This is part of the same network as Rainbow Bridge, if you have that perk. Psi Gate portals cannot be larger than the structure they’re built into.</p>
    `,
    whitelist: ['Colony', 'Nexus'],
  },
  {
    uid: 'S2YXp',
    title: 'White Rabbit',
    cost: 50,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040716164680126654/Screenshot_6.jpg',
    desc: `
    <p>The presence of at least a Colony in a Creep zone applies the effects of any of the five sensory Lures to any/all connected creep. This Expansion cannot apply the effects of sensory Lures that you haven’t purchased.</p>
    <p>Faerie Feast makes any food that creep zone produces, whether plant or animal, as addictive as food you prepared. Love Spot and Alluring Whisper allow for visual and auditory hallucinations, respectively, with hypnotic effects. Potpourri allows fine control of scents throughout the entire region. Finally, Sticky Fingers allows the creep’s flora to detain and ravish intruders.</p>
    `,
    whitelist: ['Nexus'],
  },
  {
    uid: 'usrnj',
    title: 'Stained Glass Sky',
    cost: 40,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040709521946980442/Screenshot_5.png',
    desc: `
    <p>Hives now alter the climate in your Creep zone to better fit the aesthetics of your symbiote theme. Your swarm will be able to shrug off any extreme weather or other hazards that this produces. Intruders will not be so fortunate, and will all find their tiers reduced by 1, to a minimum of 1. This stacks with the tier debuffs from Fortresses.</p>
    `,
    whitelist: ['Hive', 'White Rabbit'],
  },
  {
    uid: 'DEilk',
    title: 'Second-Generation Symbiote',
    cost: 30,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://wallpapercave.com/wp/wp5755421.jpg',
    desc: `
    <p>This adds a number of upgrades to the symbiote’s functionality. The lowest tier of targets that can fight off a symbiote larva will increase by 1 for each effective tier (after step 6) that the queen is above 7: Tier 6 targets can no longer fight off a Tier 8 queen’s Skitterers, for example, while only Tier 10 and Χ targets can fight off Skitterers produced by a Tier Χ queen. On the target side, use their effective tier after step 6, but skip steps 4 and 5.</p>
    <p>Additionally, symbiote larvae now emit a weak psychic field that reduces a target’s willingness to resist future attacks every time that individual fights one off. Each stack of this effect will last 7 days (168 hours), but there is no cap on the number of stacks that can be applied at once.</p>
    `,
    whitelist: ['Nexus'],
  },
  {
    uid: 'bcIvr',
    title: 'Royal Honey',
    cost: 20,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040713231846609006/22.png',
    desc: `
    <p>Your Colonies now produce a highly-nutritious honey-like substance, a vital material for the promotion of new queens. Ingesting one half-liter of Royal Honey over the course of a single day will apply the same physical transformation that marks your king or queen to any humanoid of the appropriate gender who does so, following all of the same rules. This includes the extra +1 tier. Under normal conditions, this effect only lasts for 14 days (332 hours) after the initial sip and then recedes the next time the drinker sleeps.</p>
    <p>However, a female humanoid who experiences this effect while installed into a Hatchery, whether or not she was already affected when she stepped in, will become a Support Queen, equal in rank and benefitting from all of the same rules as your original queen. The promotion to Support Queen is permanent. The number of support queens you can have at any time is equal to 1 for each Colony, plus 10 for each Hive.</p>
    `,
    whitelist: ['Colony', 'Evolution Chamber', 'Buzzers'],
  },
  {
    uid: 'TjHX4',
    title: 'Imperial Honey',
    cost: 40,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1024774391978004551/images_-_2022-09-29T023039.438.jpg',
    desc: `
    <p>This variant of Royal Honey, produced only in Hives and in far smaller amounts, is even more nutritious than the regular kind. The same half-liter dose will instead take its drinker to target sizes of 180% on the scales defined in Advanced Tempest Runes (Body), with up to 20% error in either direction. This transformation will last for 14 days (332 hours) after the initial sip and then recede down to Royal Honey levels the next time the drinker sleeps. The drinker will only return to their original proportions after another two weeks.</p>
    <p>The first swarm queen, whether your original or a support queen, to consume 20 liters of Imperial Honey will become your swarm’s Empress, ranked above all other queens in your swarm’s hierarchy. This can only happen once, so make it count. The Empress gains +1 to her effective tier, stacking with the queen bonus. If she is already Tier 10, her power will double, but her effective tier will not increase any further. These permanent benefits will also apply to a swarm king who has this perk and consumes enough honey.</p>
    <p>Any clutch that an Empress lays, whether personally or while installed in a Lair, will hatch in half of the usual time. If a support Queen qualifies for her own symbiote theme as per the regular rules, but your Empress has already imposed her own theme on the entire swarm, the two themes will hybridize in a natural way, but only in the lesser queen and her subordinates.</p>
    `,
    whitelist: ['Hive', 'Evolution Chamber', 'Royal Honey'],
  },
  {
    uid: 'yhrPM',
    title: 'Third-Generation Symbiote',
    cost: 40,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1043497058553823242/ww.png',
    desc: `
    <p>The Globsters’ ability is now part of the basic functions available to all of your humanoid swarm members, bringing them a step closer to the original wild shoggoths that were used to create Globsters. All humanoid swarm members may now melt into a semisolid amoeboid form for better infiltration, whether by flowing through vents, along a ceiling or wall, or hitching a ride on a person.</p>
    <p>The slime form may be maintained indefinitely, obey conservation of mass, and must remain contiguous. A swarm member otherwise has full control over the form's shape, texture, and coloration. Symbiote hosts that have innate supernatural abilities of biological or genetic origin (ex. X-gene, Quirks, hereditary magic potential, alien racial abilities, etc.) retain them in their slime form. If they have enough room, they may revert to their original or symbiote forms at will.</p>
    <p>From their slime form, a humanoid swarm member may also assume a solid "armor" form around another similarly-sized being. By doing so, any symbiote host who comes into contact with a capture target can bind them on their own, without aid from a larva.</p>
    <p>The slime form flows under the target's clothes and equipment, fully encasing them and keeping them compliant with sexual stimulation while injecting symbiote transformative fluid into blood vessels close to the skin and pumping into the digestive or (for female targets) reproductive systems. In this state, the slime form can also mentally converse with the target and speak with their voice, use their slime body as natural weapons, etc., until the target is converted (captured and symbiote-bound).</p>
    <p>This conversion method obeys the same timeframe and interruption rules as a full-cocoon Shroud binding and may be hastened with all-the-way-through penetration. This will allow symbiote transformative fluid to be absorbed by every part of the target’s digestive system at once. After conversion, the two may separate to reveal the new capture's symbiote form with the same body growth as the other types of symbiote capture. Additional sex is not needed to confim this type of capture.</p>
    <p>A slime-form may also overload their wearer's mind by spiking their libido and sensitivity. This overloads their own mind as well, cannot be undone manually, and will only reverse itself when the duo are fully satisfied.</p>
    `,
    whitelist: ['Hive', 'Nexus', 'Globsters', 'Second-Generation Symbiote'],
  },
  {
    uid: 'n5KFN',
    title: 'Slime Fusion',
    cost: 20,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1045752304894226573/LlQIDqW.png',
    desc: `
    <p>If more than one member of your symbiote swarm has entered their slime form, they can combine into a larger slime with one body, multiple minds, and one will. Swarm members may freely enter and exit such fusions without any negative side-effects.</p>
    <p>If any members of such a fusion have innate biological or genetic abilities, this will catalyze the appearance of those in the others. The specific expressions of these gifts will not always be consistent between subjects. For example, a Quirk user in such a fusion would cause others in it to gain Quirks of their own, and not necessarily similar ones. This feature cannot copy Heritage powers.</p>
    `,
    whitelist: ['Hive', 'Third-Generation Symbiote'],
  },
  {
    uid: 'D1hNt',
    title: 'Universal Bus',
    cost: 50,
    category: 'Binding',
    type: 'Expansion',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040711567509045310/HD-wallpaper-alien-robot.jpg',
    desc: `
    <p>Skitterers may now target digital entities. A Skitterer changes the shape of its “tongue”  to interface with any external computer port, then projects itself into a representation of virtual space. If a virtual space already exists, the Skitterer will simply enter that instead. Once there, it may stalk and convert the locals as normal.</p>
    <p>Conversion will pull a software AI or brain upload into a new physical body at the real-world location of the Skitterer, skipping to the penultimate stage of the normal process. These bodies are often natural cyborgs or technopaths, with no loss of ability compared to their previous selves, though the exact end-states after conversion are highly variable. Again, your Evolution Chambers may only copy technological abilities from one member of your swarm to another if you have a Hive.</p>
    <p>End-users of consumer VR products are also vulnerable. Their physical bodies will be transformed on-site, and they may log out and search for their new master’s physical location on their own time. The conversion will disable any anti-logout prevention or punishment systems. Finally, existing symbiote hosts may log in and use their swarm-granted abilities in digital worlds.</p>
    `,
    whitelist: ['Nexus', 'Skitterers'],
  },
]

export const symBuildings: PerkFull[] = [
  {
    uid: '22NAS',
    title: 'Hatchery',
    cost: 30,
    dCost: 20,
    category: 'Binding',
    type: 'Freestanding',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040723170233171978/FirstSpawningPool_SC2-HotS_Art1.webp',
    desc: `
    <p>Your queen no longer has to carry the burden of new eggs herself. This fixed structure can do so instead. Any queen within your swarm may sexually interface with the spawning organ at the center of the Hatchery. This tall and narrow pool, shaped like a champagne flute and filled with water, will lock itself around the queen’s waist after she enters, leaving her upper body exposed. Any of a queen’s clothes that are caught within the organ when it locks will be heavily damaged or worse, so it’s important that she either enter while nude or banish any clothes that she wants to keep into a Pocket Space.</p>
    <p>Once in place, the organ will continuously ravish the queen as she produces a new clutch of larval eggs immediately after each orgasm, skipping the gestation period and automatically depositing each clutch into a spawning pool below the organ, until that pool is filled to capacity. Spawning pools are ideal environments for the eggs’ incubation and can hold up to 160 clutches at a time.</p>
    <p>The rest of a Hatchery’s interior may be dedicated to a variety of roles as needed. You may only have a single Hatchery at a time before you’ve built your first Lair.</p>
    <p>After 24 hours of growth, a new Hatchery will reach its maximum size of 25 meters tall, and an additional 10 vertical meters of basements underground, with a 2,000 square meter footprint. A finished Hatchery will spread Creep to about a 1 kilometer radius, centered on the spawning pool, reaching its maximum extent another 7 days (168 hours) after construction.</p>
    `,
    whitelist: ['Alterzelu Symbiote'],
  },
  {
    uid: 'gRUqm',
    title: 'Nursery',
    cost: 0,
    dCost: 5,
    category: 'Binding',
    type: 'Add-on (Hatchery)',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038872712321433631/c74ec043a0148f2cb671fb55972d5438.jpg',
    desc: `
    <p>Nurseries increase the efficiency of your unit production buildings and provide additional incubation space. Nurseries can only be built attached to a Hatchery, Burrow, Nest, Reef, or another Nursery. Even if a large mass of Hatcheries and Nurseries flow together, each Nursery will only accept eggs from a single production building at a time. A Nursery will grow to full size in 8 hours, reaching a maximum size of 5 meters tall with a 250 square meter footprint.</p>
    <p>Each Nursery can hold up to 20 egg clutches from the attached production building. This capacity increases with the highest-upgraded Hatchery in the same creep zone: 40 clutches (+20) with a Lair, 60 clutches (+20) with a Colony, and 80 clutches (+20) with a Hive.</p>
    <p>Each Nursery present within a Creep zone also decreases the incubation time for all units produced in that zone by 2.5%. After the first 20 Nurseries are present in a single Creep zone, every 20 more Nurseries in the same Creep zone will cut the incubation time in half again, with no limit.</p>
    `,
    whitelist: ['Hatchery'],
  },
  {
    uid: 'cRbd0',
    title: 'Grove',
    cost: 0,
    dCost: 5,
    category: 'Binding',
    type: 'Freestanding',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038873759140020345/88070758ecb46e2c4bbeca48c21c8b7a.jpg',
    desc: `
    <p>Groves serve as parkland for any wildlife in your Creep zones or sapient subjects who enjoy the outdoors. Groves also make a Hatchery’s creep more nutritious, decreasing the maturation time for all units produced in that Creep zone.</p>
    <p>Every 5 Devotion Points dedicates 40,000 square meters of land to Groves and reduces the maturation time for larvae by 2.5%. The dedication process takes only 3 hours. After the first 100 Devotion Points are invested into Groves in a single Creep zone, every 100 more in the same Creep zone will cut the maturation time for larvae in half again, with no limit. Units produced at Burrows, Nests, and Reefs require ten times as much Grove land in a single Creep zone to achieve the same reduction.</p>
    `,
    whitelist: ['Hatchery'],
  },
  {
    uid: 'OJGJK',
    title: 'Den',
    cost: 0,
    dCost: 10,
    category: 'Binding',
    type: 'Freestanding',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038872858815909949/d49u5y6-214ff8b8-973d-42b5-b2b4-77d46185c311.jpg',
    desc: `
    <p>Dens provide shelter to your sapient subjects and act as structural foundations for later, larger structures.</p>
    <p>After 12 hours of growth, a new Den will reach its maximum size of 10 meters tall, and an additional 5 vertical meters of basements underground, with a 500 square meter footprint.</p>
    `,
    whitelist: ['Hatchery'],
  },
  {
    uid: 'EG3MX',
    title: 'Lair',
    cost: 85,
    dCost: 200,
    category: 'Binding',
    type: 'Upgrade (Hatchery)',
    image: 'https://cdnb.artstation.com/p/assets/images/images/002/102/807/large/henry-ledesma-the-hive.jpg?1457285526',
    desc: `
    <p>The Lair is a direct upgrade to the Hatchery, with all of the features and abilities of its predecessor. It can incubate up to 1600 clutches at a time, plus the effects of attached Nurseries. Upgrading a Hatchery to a Lair whitelist an additional 180 Devotion Points as construction material, minus any recycled from other swarm structures within the Lair’s new footprint. At this stage, you may build additional Hatcheries, but only upgrade a single one to a Lair.</p>
    <p>After 3 days (72 hours) of growth, the new Lair will reach its maximum size at 50 meters tall, and an additional 20 vertical meters of basements underground, with a 10,000 square meter footprint. A finished Lair will spread Creep to about a 5 kilometer radius, centered on the spawning pool, reaching its maximum extent another 2 weeks (336 hours) after construction.</p>
    <p>The presence of at least a Lair in a Creep zone allows the Creep to assimilate stationary, non-sapient creatures, converting them into your familiars and enforcing your swarm’s aesthetics upon them. Standard reproduction for assimilated species will continue within your swarm, allowing you to take advantage of any useful traits their evolution creates without micromanaging their populations.</p>
    <p>You may install a mature female subject, not already Devoted in a building’s construction or elsewhere, into the spawning organ of a Lair on a longer-term basis. If a non-queen is installed, her clutches will take 50% longer to hatch than the queen’s clutches: at the base rate, +7 days. Either way, new clutches will be produced as room allows.</p>
    <p>Queens may remove themselves from this organ at will, but removing a non-queen takes 24 hours, whitelist your consent, and will halt all new egg production at that site during that time. The spawning organ is a very intense experience, with sustained installation causing both physical and mental side-effects. Body Tune-Up and Stress, Addiction, and Mind Defenses are highly recommended.</p>
    `,
    whitelist: ['Den'],
  },
  {
    uid: '26pNK',
    title: 'Burrow',
    cost: 0,
    dCost: 60,
    category: 'Binding',
    type: 'Upgrade (Den)',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1038875154568183939/insect_hive_by_dentifrix_dd9otnd-fullview.jpg',
    desc: `
    <p>The Burrow is the primary production building for non-sapient land units. Upgrading a Den to a Burrow whitelist an additional 50 Devotion Points as construction material, minus any recycled from other swarm structures within the Burrow’s new footprint. After 48 hours of new growth, a new Burrow will reach its maximum size of 15 meters tall, and an additional 20 vertical meters of basements, with a 4,500 square meter footprint.</p>
    <p>Burrows have much the same spawning interface as Lairs, with a specialized organ that a mature female subject can install herself into and create new eggs. A Burrow’s eggs are limited to non-larval land units only, whether the pre-designed options, local species assimilated into your swarm, or creatures that your geneticists and bioengineers have custom-designed at an Evolution Chamber.</p>
    `,
    whitelist: ['Lair'],
  },
  {
    uid: '4khWM',
    title: 'Nest',
    cost: 0,
    dCost: 80,
    category: 'Binding',
    type: 'Upgrade (Den)',
    image: 'https://i.pinimg.com/originals/c1/97/98/c197987a80635987ee4836634d78ea0e.jpg',
    desc: `
    <p>The Nest is the primary production building for non-sapient air units. Upgrading a Den to a Nest whitelist an additional 70 Devotion Points as construction material, minus any recycled from other swarm structures within the Nest’s new footprint. After 48 hours of new growth, a new Nest will reach its maximum size of 65 meters tall, and an additional 10 vertical meters of basements, with a 2,100 square meter footprint.</p>
    <p>Nests have much the same spawning interface as Lairs, with a specialized organ that a mature female subject can install herself into and create new eggs. A Nest’s eggs are limited to non-larval air units only, whether the pre-designed options, local species assimilated into your swarm, or creatures that your geneticists and bioengineers have custom-designed at an Evolution Chamber.</p>
    `,
    whitelist: ['Lair'],
  },
  {
    uid: 'eJ8Gw',
    title: 'Reef',
    cost: 0,
    dCost: 80,
    category: 'Binding',
    type: 'Upgrade (Den)',
    image: 'https://2dartistmag.com/wp-content/uploads/2016/02/final_image-2.jpg',
    desc: `
    <p>The Reef is the primary production building for non-sapient sea units. Upgrading a Den to a Reef whitelist water access and an additional 70 Devotion Points as construction material, minus any recycled from other swarm structures within the Reef’s new footprint. After 48 hours of new growth, a new Reef will reach its maximum size of 20 meters tall, and an additional 10 vertical meters of basements, with a 5,250 square meter footprint.</p>
    <p>Reefs have much the same spawning interface as Lairs, with a specialized organ that a mature female subject can install herself into and create new eggs. A Reef’s eggs are limited to non-larval sea units only, whether the pre-designed options, local species assimilated into your swarm, or creatures that your geneticists and bioengineers have custom designed at an Evolution Chamber.</p>
    `,
    whitelist: ['Lair'],
  },
  {
    uid: 'tr0zP',
    title: 'Evolution Chamber',
    cost: 0,
    dCost: 50,
    category: 'Binding',
    type: 'Freestanding',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1043493854806032404/dd3714m-558ad435-ea4e-4204-bc91-83cc764d391d.jpg',
    desc: `
    <p>Evolution Chambers are specialized workshops for your geneticists, fleshcrafters, and bio-manipulators, where they can create new breeds of warbeast for your armies. After 48 hours of growth, an Evolution Chamber will reach its full size of 15 meters tall, and an additional 10 vertical meters of basements, with a 6,000 square meter footprint.</p>
    <p>At an Evolution Chamber, your design teams may freely mix, match, and customize biological features from any species that your creep has assimilated, in addition to features from any of the default larva types available to your swarm, any pre-designed symbiote units available to your swarm, and your queens’ inborn supernatural traits. Upgrades are not auto-applied to all eligible recipients. Sapient subjects must visit an Evolution Chamber as individuals for their modifications. Major units are instead recycled at the same type of production building (not necessarily the exact building) that spawned them.</p>
    <p>The basic symbiote biosuit can also be customized, whether for general use or individual preferences. Biological traits that have been integrated into a biosuit can always be copied to other swarm members, depending on the bioengineer’s skills. Psionic traits can be copied if you have a Nexus, while technological and mystical traits can only be copied if you have a Hive, again depending on the engineer’s skills. These traits will breed true.</p>
    <p>Previous experience is not required for personnel assigned to your Evolution Chambers. The design interface is simple and intuitive, with clear tutorials, and allows for extreme creativity. If you have Science Talent, the interface will count as a trainer of Tier 5 in this field. The presence of a Colony in the same Creep zone raises this tier level to 6, a Hive raises it to 7, and a World Tree raises it to 8.</p>
    <p>All designs are filtered through the theming of the queen whose portion of the swarm actually produces them as units. If you have multiple queens, individuals in your swarm may also come to an Evolution Chamber to be reassigned and rethemed from one queen’s part of the overall swarm to another.</p>
    `,
    whitelist: ['Lair'],
  },
  {
    uid: '2TSbR',
    title: 'Guard Spire',
    cost: 0,
    dCost: 30,
    category: 'Binding',
    type: 'Freestanding',
    image: 'http://2.bp.blogspot.com/-HKtvApKZZPY/TdwiuWiyzAI/AAAAAAAAAgw/UJN-JfowAfU/s1600/04+-+clouds.jpg',
    desc: `
    <p>This is your basic defensive emplacement, a tower 60 meters tall, with an additional 10 vertical meters of basements underground and a 250 square meter footprint. Spires are suitable sites from which swarm members can observe their surroundings or use their own weaponry against intruders.</p>
    <p>With an Evolution Chamber, your swarm’s bioscientists can adapt any special abilities allowed at that structure into new Spire variants. With a Nexus, psionic abilities become viable as well, and mystical and technological traits with a Hive. The more complex or esoteric the ability, the harder it is to adapt and the more expensive the variant becomes in Devotion Points.</p>
    <p>A basic Guard Spire can support abilities that are appropriate for a combatant of up to Tier 4. The Colony stage allows Guard Spires to be upgraded to support abilities that are appropriate for a combatant of up to Tier 5. This whitelist an additional 420 Devotion Points over the basic Guard Spire. The Hive stage allows Guard Spires to be upgraded to support abilities that are appropriate for a combatant of up to Tier 7. This whitelist an additional 6,300 Devotion Points over the Colony-stage version.</p>
    `,
    whitelist: ['Lair'],
  },
  {
    uid: 'LjU3E',
    title: 'Colony',
    cost: 115,
    dCost: 2000,
    category: 'Binding',
    type: 'Upgrade (Lair)',
    image: 'https://cdnb.artstation.com/p/assets/images/images/001/486/647/large/david-levy-formic-planetsurface-abovefactory-a-02.jpg?1447263416',
    freebies: {
      talentPerks: [{ title: 'Psychic Talent', cost: 0, count: 1 }, { title: 'Talent Sharing', cost: 0, complex: [{ flavor: 'Psychic Talent', cost: 0 }], count: 1 }],
      defensePerks: [],
    },
    desc: `
    <p>The Colony is a direct upgrade to the Lair, with all of the features and abilities of its predecessor. It can incubate up to 12,000 clutches at a time. Upgrading a Lair to a Colony whitelist an additional 1,800 Devotion Points as construction material, minus any recycled from other swarm structures within the Colony’s new footprint. At this stage, you may build additional Lairs, but only upgrade a single one to a Colony.</p>
    <p>After 9 days (216 hours) of growth, the new Colony will reach its full size at 100 meters tall, with an additional 40 vertical meters of basements underground, with a 50,000 square meter footprint. A finished Colony will spread Creep to about a 25 kilometer radius, centered on the spawning pool, reaching its maximum extent another 3 weeks (504 hours) after construction.</p>
    <p>Once you’ve purchased access to Colonies, you and all symbiote-bound subjects will benefit from Psychic Talent, as if you had purchased Talent Sharing (Psychic). (Take Psychic Talent and Talent Sharing (Psychic) for free.)</p>
    `,
    needed: 2,
    whitelist: ['Evolution Chamber', 'Burrow', 'Nest', 'Reef'],
  },
  {
    uid: 'EuVg4',
    title: 'Nexus',
    cost: 0,
    dCost: 400,
    category: 'Binding',
    type: 'Freestanding',
    image: 'https://cdn.discordapp.com/attachments/925963686433132644/1040718230592962630/Screenshot_7.jpg',
    desc: `
    <p>The Nexus is a focal point for your swarm’s psionic power and foundation for further growth. It is required for most psychic Expansions and allows all supernatural abilities present in your swarm to be added to your unit and structure designs. After 7 days (168 hours) of growth, a new Nexus will reach its full size at 30 meters tall, and an additional 20 meters of basements underground, with a 24,000 square meter footprint.</p>
    <p>A Nexus is always a suitable training site for supernatural abilities. If a sapient member of your swarm trains a supernatural ability at one, the appropriate Basic Talent will instead scale to the most potent sapient swarm member in the field being trained, regardless of whether or not they are physically present or actively participating in any training exercises.</p>
    `,
    whitelist: ['Colony'],
  },
  {
    uid: 'GCJsO',
    title: 'Fortress',
    cost: 0,
    dCost: 3000,
    category: 'Binding',
    type: 'Freestanding',
    image: 'https://cdnb.artstation.com/p/assets/images/images/001/486/527/large/david-levy-fr-formiccity-ilo-120316-sr0350-1120-01-dl.jpg?1447261903',
    desc: `
    <p>This is your mid-level defensive emplacement, about 85 meters tall, with an additional 40 vertical meters of basements underground, and a 24,500 square meter footprint. A fully-grown Fortress will spread Creep to about a 20 kilometer radius, reaching its maximum extent another 3 weeks (504 hours) after construction. Fortresses must be grown on existing Creep beds.A Fortress’s interior has an excess of internal space, so feel free to dedicate it to a variety of administrative or military roles as needed.</p>
    <p>Like Guard Spires, Fortresses are always suitable sites from which swarm members can observe their surroundings or use their own weaponry against intruders. With an Evolution Chamber, your swarm’s scientists can adapt any special abilities found in the biology of your swarm members - both the non-sapients assimilated by your swarm’s Creep and your sapient subjects - into new Fortress variants. With a Nexus, psionic abilities become viable as well, and mystical and technological traits with a Hive Fortresses can support abilities that are appropriate for combatants of up to Tier 6.</p>
    <p>Additionally, Fortresses impose an attrition debuff on intruders who enter a radius of 5 kilometers around them. Without additional upgrades, blue- and copper-star intruders will find their tiers reduced by 1, to a minimum of 1. This applies at step 6 and does not stack with additional Fortresses or Redoubts.</p>
    `,
    whitelist: ['Colony'],
  },
  {
    uid: 'ys3bz',
    title: 'Hive',
    cost: 145,
    dCost: 20000,
    category: 'Binding',
    type: 'Upgrade (Colony)',
    image: 'https://cdnb.artstation.com/p/assets/images/images/001/486/653/large/david-levy-formic-planetsurface-d-01.jpg?1447263430',
    desc: `
    <p>The Hive is a direct upgrade to the Colony, with all of the features and abilities of its predecessor. It can incubate up to 80,000 clutches at a time. Upgrading a Colony to a Lair whitelist an additional 18,000 Devotion Points as construction material, minus any recycled from other swarm structures within the Hive’s new footprint. At this stage, you may build additional Colonies, but only upgrade a single one to a Hive.</p>
    <p>After 27 days (648 hours) of growth, the new Hive will reach its full size at 200 meters tall, and an additional 80 vertical meters of basements underground, with a 250,000 square meter footprint. A finished Hive will spread Creep to about a 125 kilometer radius, centered on the spawning pool, reaching its maximum extent another 4 weeks (672 hours) after construction.</p>
    `,
    needed: 1,
    whitelist: ['Nexus', 'Fortress'],
  },
]

export const symbiotePerksObject = symbioteBinding.reduce((a, x) => { a[x.title] = x; return a }, {} as Record<string, PerkFull>)
export const symbioteBuildingsObject = symBuildings.reduce((a, x) => { a[x.title] = x; return a }, {} as Record<string, PerkFull>)
