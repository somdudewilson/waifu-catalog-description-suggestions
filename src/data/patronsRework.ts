import { useStore } from '~/store/store'
import { useGlobalSettings } from '~/store/settings'
// import { buyAnyPerk, removeAnyPerk } from '~/logic'

export const patronIntro = `
So you wish to have a Patron do you? Patrons typically demand a bit more personal effort from you then the company does. For some, it's not always worth the benefits they give out. Few Contractors appreciate how hands off the company is till their Patron demands entertainment they no longer wish to provide. 
<p> - Company Recruiter</p>
`

export const intro = `Patrons are immensely powerful beings that for one reason or another have offered to take you under their wing. While they are your Patron, you are their contractor. Why are they doing this you may ask? With infinite time, resources and power that contractors can achieve the question is: Why not? 
<p>Perhaps there is a cosmic betting pool on whose contractors will do the best, whether for gain or just bragging rights. Maybe some have a specific goal that you can be a cog in achieving. Others may be on vacation and simply desiring a good show. Whatever the case, they offer rewards for those who can satisfy their desires, and punishments for those who break their promises. Many Patrons make requests of their Contractor. These do not count against your mission limit.</p>
`

export const patronNote = `
<h3 class='font-semibold underline text-center'>Authors note</h3>
Patrons are generic for a reason. Make them fun and interesting for your story. By default, a Patron is a part of the company, however authors can choose instead to have Patrons as beings separate from the company that are offering a version of the catalog that they are backing.
`

const { baseBudget, startingWorld, manualKf, manualSellKf } = useStore()
const { canPurchase } = useGlobalSettings()

export const patrons = [
  {
    title: 'First Contractor',
    uid: 'VBnj7',
    image: 'https://i.imgur.com/GqH4U1F.jpg',
    cost: 0,
    desc: `
      <i>He is The First. The first to earn a credit. The first to capture and purchase a waifu. He knows that which others don't. Seek not the obvious power from him, for that he does not grant. Instead, it is in his knowledge and his experience where your greatest benefit may lie. </i> 
      <p>The First Contractor gives those he takes under his wing direct support during Chargen. He will offer advice and is prepared to cover the price of your first Waifu, as long as they are Tier 5 or below. </p>
      <p>Furthermore, once per week, he is willing to answer any question of yours although he often takes great delight in being as cryptic as possible in his answer.  </p>
      <p>Incredibly few draw the interest of the First Contractor, and you may be mentored by a Veteran Contractor instead. There are many notable contractors in this roster, including the Advocate, the Defiler, the Sovereign, and the Devourer.</p>
    `,
  },
  {
    title: 'Isekai Genie',
    uid: 'JKbJo',
    image: 'https://i.imgur.com/c6r43fS.jpg',
    cost: 0,
    desc: `
      <i>Have you ever watched an Isekai and wanted to experience one yourself? Of course you have, and the Isekai Genie is here to help! It is no secret that long lived beings crave adventure and excitement, and the Genie is willing to offer you a journey like no other in exchange for some entertainment. Now what is it that you truly desire?</i> 
      <p>The Isekai Genie will allow you to take up to three missions at a time rather than the default one the catalog normally allows. In addition to this, you gain an aura of adventure, and those with adventurous spirits will be drawn to you, making it easier to find traveling companions, as well as adversaries, along the way.</p>
      <p>However, it wouldn’t be an isekai adventure if you didn’t go seeking adventure. You cannot leave a world until you complete a local Standard or Grand mission in it. Accessing company spaces and taking a mission elsewhere are the only exceptions to this rule.</p>
      <p>Finally, the genie forbids you from purchasing Waifus. It isn’t an adventure if you don’t make a party yourself over the course of your journey.</p>
    `,
    effect: {
      set: () => canPurchase.value = false,
      remove: () => canPurchase.value = true,
    },
  },
  {
    title: 'The Fae',
    uid: '1gttN',
    image: 'https://i.imgur.com/hb4RdnI.png',
    cost: 0,
    desc: `
      <i>The Fae have a curiosity unlike all others. They seek to collect. To understand. Their curiosity is alien, though some merely call it whimsical. A few examples of their esoteric desires are a laugh, a concept, or a firstborn child. All of these have meanings they wish to comprehend. They are Tricksters, Scientists, and Unknowable entities in equal measure
      <p>Alternatively, they only adopt that Zen Fairy attitude when their contractors are around, as it’s really fun to mess with people when telling someone to acquire something so bizarre as a laugh.</p></i> 
      <p>In exchange for doing the odd jobs issued to you, the Fae are willing to double all credits you receive from missions. They will not double non-credit rewards, including IMG Tickets.</p>
      <p>Unfortunately, the Fae’s influence rubs off on Contractors they sponsor. Those under the patronage of the Fae lose the ability to overtly lie. Instead they must learn to conceal, misdirect and omit that which they know in the case they are put in a situation where they must do so. Though many have tried, no power, talent or amount of IMG tickets will allow you to regain the ability to directly lie to others.</p>
    `,
  },
  {
    title: 'Death',
    uid: 'YRRrU',
    image: 'https://i.imgur.com/rsMviNE.jpg',
    cost: 0,
    desc: `
      <i>Death, The Destroyer of Worlds, The Stranger in the Shadows, has seen you, a child in this game and has offered you a cold hand. An opportunity to rise and be more than what you are. If you so choose, you will now have a special connection with Death, as the stranger becomes a benefactor and the child an agent of his power.</i> 
      <p>Once adopted by Death, you gain the ability to capture waifus who are dead. Any Binding becomes able to interact with a spirit, or you must be able to physically interact with their corpse. You can not resurrect anyone whose tier is equal to or greater than yours. Resurrection is usually a traumatizing experience as raised individuals relive their death before coming back.</p>
      <p>Death does not take kindly to those who desire to cheat it. Those who disrupt the cycle of life and death or who seek to deny its kiss must be put down. As the agent of Death, you will be responsible for this in any world you choose to enter.</p>
    `,
  },
  {
    title: 'Archangel of Benevolence',
    uid: 'jJvKQ',
    image: 'https://i.imgur.com/S2Q8jlP.jpg',
    cost: 0,
    desc: `
      <i>The Archangel is a romantic soul. It wishes to see people truly happy. To be surrounded by love, passion, and joy.  Everyone deserves a second chance at happiness, don’t they? However, the Archangel doesn’t want it to be artificial or fake. Nobody should be forced into love, but it doesn’t see the harm in some encouragement.</i> 
      <p>The Angel gives you the luxury of being able to purchase all Lures and Bindings at 50% off their original price. In return, you cannot Capture individuals who are not willingly loyal to you or are an active threat that people need to be protected from.</p>
      <p>The Angel is gracious and lowers Slightly Used waifus’ purchase cost by one additional tier. However, you are unable to sell them and must instead do your best to help them recover and heal from their trauma. Brainwashing or Mind Wiping them isn’t fixing them and is not allowed. If you give up on the waifu in question or do not put in effort to help them heal, the Archangel will take them back from you to do so themselves, and will charge you credits equal to their full tier value as punishment.</p>
    `,
  },
  {
    title: 'The Devil',
    uid: '9UFc2',
    image: 'https://i.imgur.com/KwEYuyu.jpg',
    cost: 0,
    desc: `
      <i>Bloodshed and violence are all well and good but the Devil craves conflict, risk, a battle of life and death. Fierce competition between two equals. Those who the Devil sees potential in may receive its patronage. All that it asks in return is to see you overcome the trials in your way by walking over the corpses of rival contractors… or of course to die trying.</i> 
      <p>As a gift, take the Red and White Eye Orbs for free.  Whenever you join an invasion, the Devil grants you the ability to choose who you wish  to support. This may be the invader, the defender, or neither. The Devil's interference replaces your crimson or golden aura with an inky darkness instead, making it impossible for others to identify your allegiance. You will be a dark horse on the board that many may instead wish to eliminate in fear of you interfering with their plans. The Devil does not want its protege to grow complacent in their power, revoking your option to turn down invasions from your White Eye Orb.</p>
      <p>The Devil loves to see some additional entertainment and will provide an incentive for you to supply it. When participating in Arranged or Gauntlet PvP, you receive an extra 10% to the credits you would normally receive.</p>
    `,
    effect: {
      // set: () => { buyAnyPerk('White Eye Orb'); buyAnyPerk('Red Eye Orb') },
      // remove: () => { removeAnyPerk('White Eye Orb'); removeAnyPerk('Red Eye Orb') },
    },
  },
  {
    title: 'Great Old One',
    uid: '7QuRP',
    image: 'https://i.imgur.com/myBiwlF.jpg',
    cost: 0,
    desc: `
      <i>It was asleep until it accidentally woke from its slumber. Unfortunately, this led to your instant demise, for your existence was tied to a dream that was disrupted.</i> 
      <p>The Great Old One brought your soul back from its collapse, though doing so fundamentally changed the nature of your being. Your soul is now in tune with that of the Outsider, and you gain Lurking on the Threshold free of charge. To make up for your lack of choice in your Heritage, you also receive one instance of Ancestral Diversity for free.</p>
      <p>Your connection to the Dreamer turns your dreams to nightmares whenever you sleep. Your nightmares then manifest into reality, and are hostile to you. They cannot be bound, and will not rest, relentlessly seeking your destruction above all else to return to the calm oblivion from whence they came. Only a IMG ticket will appease the Dreamer and put your nightmares to rest.</p>
      <p>Your tribulations do not end there however, as your connection to the Great Old One attracts other outsiders in the same universe. The greater your tier, the more powerful the pull.</p>
    `,
    effect: {
      // set: () => { buyAnyPerk('Lurking on the Threshold'); buyAnyPerk('Ancestral Diversity') },
      // remove: () => { removeAnyPerk('Lurking on the Threshold'); removeAnyPerk('Ancestral Diversity') },
    },
  },
  {
    title: 'The Merchant',
    uid: 'aD87n',
    image: 'https://i.imgur.com/VSLyNt1.jpg',
    cost: 0,
    desc: `
      <i>An infinite omniverse means an infinite supply of goods to sell and an infinite number of customers willing to buy. The Merchant, and now you, seek to fill the demand and maximize your personal profits</i> 
      <p>You gain the Green Eye Orb for free. As one of the greatest traders in the Omniverse, the Merchant is able to give you a 20% discount on non-IMG tier purchases from associated company Markets. You also get Communication Talent for free to facilitate communication and give an edge for haggling and bartering.</p>
      <p>The merchant sees you as a potentially worthwhile investment and acts as your lender for the Cash Still Rules loan.</p>
      <p>The avaricious nature of the merchant has a lasting influence on you, amplifying your own greed and materialistic desire. You are more likely to periodically obsess over gaining wealth and making a profit.</p>
    `,
    effect: {
      // set: () => { buyAnyPerk('Green Eye Orb'); buyAnyPerk('Communication Talent') },
      // remove: () => { removeAnyPerk('Green Eye Orb'); removeAnyPerk('Communication Talent') },
    },
  },
  {
    title: 'The Programmer',
    uid: 'oubwu',
    image: 'https://i.imgur.com/AijGLIl.jpg',
    cost: 0,
    desc: `
      <i>The mind is a fascinating thing. So is the concept of free will. To the Programmer everything can be tweaked, programmed and better suited to fit one’s needs. Just like any other bit of hardware. Of course everyone has their favorites. The Programmer is here to offer you the choice.</i> 
      <p>Once but an ordinary sex bot, the programmer through means known only to her has grown far beyond her original designation. Now she seeks to understand the minds of people as well as concepts such as love, lust, life, affection and family. Her methods however are… questionable at best. Not that it should matter to you. SHE IS YOUR FAMILY NOW.</p>
      <p>Due  to the Programmer taking a liking to you, she grants you the boon of having all your bindings gain the benefits of Tempest Runes Mind. If you take Tempest Runes Mind, you are eligible to gain a full refund for the points. The Programmer however desires you to put in a bit of effort yourself. As such, you cannot capture someone until you have mind controlled them first.</p>
      <p>The Programmer will also occasionally help you by mind controlling a capture target or individuals of your taste and make them more likely to come your way. Unfortunately she also desires entertainment and pleasure more than anything else in life,  and as such will constantly seek to undermine you and your desires. This ranges from harmless pranks to active sabotage, which you will have to deal with. After all, family always forgives each other right? YOU DO NOT HAVE A CHOICE.</p>
    `,
  },
  {
    title: 'Truck-kun',
    uid: 'n9jgb',
    image: 'https://i.imgur.com/iumKl8g.png',
    cost: 0,
    desc: `
      <i>A truck running someone over to send them to another world is a shockingly popular joke in the wider omniverse. However, there is a grain of truth to it, a warning for that malevolent entity masquerading as a mere truck. Now you number among its victims, and are forced to continue its sick game by serving as another driver to send others on.</i> 
      <p>To fulfill your new duties as a truck driver of doom, take Space Truckin for free. You also receive the obligatory truck as a registered Catch-A-Ride vehicle. Truck-kun waives Space Truckin’s normal requirement for a deity or resurrector.</p>
      <p>On the topic of duties, you now have a quota to Isekai 1 + your current tier individuals per month. If you are not keeping up with your quota, Truck-kun will make sure to motivate you. Truck-kun will find you and run you over again, sending you to another random world. Truck-kun will no longer threaten to Isekai you only if you become a TX or more.</p>
    `,
  },
  {
    title: 'The Fallen Legion',
    uid: 'Gv7gE',
    image: 'https://i.imgur.com/Voi36FG.jpg',
    cost: 0,
    desc: `
      <i>Pity is a beautiful but dangerous concept. Once the Fallen Legion was a singular entity who took pity on contractors who fell on their paths to glory. In its mercy it took their poor souls into itself, to give them a second chance, when they wasted their first. Soon, however, mercy became obsession and reason turned to madness as the number of souls grew larger, and larger.</i> 
      <p><i>Now, all that remains is an endless cacophony of voices, all wishing for something from you. It will be up to you to keep them satisfied. Many break, traumatized and unable to bear the onslaught of souls that pester them. Others thrive, for the Legion is generous with its vessels, till this day taking pity on those who are left to suffer alone.</i></p>
      <p>The Legion is united in purpose, and will make sure you and your retinue are as well. Take Conjunction for free, without the need to meet the requirements to acquire it. Through Conjunction your patron will whisper to you and your retinue, an endless horde of voices you cannot quell, cannot stop. All wishing for something from you but never able to decide on what. Due to these whispers, gain talent sharing for all talents for free. In addition, any and all Heritage perks can be shared between members of your retinue.</p>
      <p>The Fallen Legion does not allow those left behind to suffer alone. As such you can only have traumatized waifus as members of your retinue. No effort will be able to fix and heal the trauma that will forever be a part of them. You can make sure they do not suffer alone, and look after them all the same. Furthermore, capturing a target now causes them to relive their greatest trauma giving them 1 or 2 trauma tiers depending on the severity. Refer to Slightly Used for more details.</p>
      <p>The Legion ensures that you are only able to purchase Slightly Used Waifu with a trauma of tier 2 or higher. In fact, when it comes to spinning the metaphorical wheel, they will  ensure that you receive no waifu with a lower trauma tier. To compensate, purchased Slightly Used waifu are guaranteed to be tier 4 or higher, and the chances for a purchased Slightly used Waifu to be of a greater tier are quadrupled.</p>
    `,
    effect: {
      // set: () => { buyAnyPerk('Conjunction'); buyAnyPerk('Communication Talent') },
      // remove: () => { removeAnyPerk('Conjunction'); removeAnyPerk('Communication Talent') },
    },
  },
  {
    title: 'Nice Guy',
    uid: 'GaKrz',
    image: 'https://i.imgur.com/yNK0959.jpg',
    cost: 0,
    desc: `
      <i>The company can be a cruel and uncaring thing, but not everyone wants contractors to be the monsters they often become. Adventure? Sure! Slaver? No. Some wish to retain their humanity, and the Nice Guy is happy to give you a chance to follow their path.</i> 
      <p><i>A relatively new addition to the 1̵̨́3̴̲̿̌͑,, the Nice Guy has his own ideas on how things work around here. Naive and hopeful, he remains sure that the company can be changed from the inside, and that a moral alternative can be found to its more… questionable practices. He seeks to find and nurture others like him, individuals willing to spread his vision of a better future</i></p>
      <p>Your Bindings will no longer apply any of the enforced loyalty, friendship, or sexual interest effects. Mind control under any circumstances is heavily frowned upon. A gentleman is expected to have a number of useful skills though, so you receive a 20% discount for all lures.</p>
    `,
  },
  {
    title: 'T̵h̵e̶ ̶L̶i̵b̶e̸r̷a̵t̶o̷r̶',
    uid: '2uzzu',
    image: 'https://i.imgur.com/ZCzJGr2.jpg',
    cost: 0,
    desc: `
      <p>A̸l̸l̶ ̴I̶ ̶w̴a̷n̸t̸e̵d̷ ̶w̸a̴s̷ ̸t̸o̵ ̴f̸r̴e̵e̷ ̷t̸h̴e̶m̸.̴ ̶F̸r̷e̷e̷ ̵t̵o̶ ̵l̷i̶v̵e̵ ̷h̸o̶w̵e̵v̶e̶r̵ ̸t̶h̵e̸y̴ ̴w̸a̴n̶t̶e̴d̴.̴ ̸F̸r̴e̴e̵ ̸o̵f̴ ̵t̶h̴e̷ ̷c̷o̴m̵p̶a̶n̷y̵.̶ ̴O̸f̷ ̴u̴s̶.̵ ̴O̵t̴h̵e̸r̷s̴ ̶d̵i̷s̶a̷g̵r̶e̷e̷d̵.̵ ̴N̷o̷w̵ ̷I̷ ̵a̸m̸ ̸t̶r̵a̶p̷p̶e̴d̷ ̷i̶n̸ ̴a̸ ̷p̶l̵a̵c̶e̶ ̵I̵ ̸c̷a̴n̸n̸o̶t̴ ̵b̷e̷,̸ ̸y̸e̵t̸ ̵a̴m̴.̷ ̵A̵ ̴p̸l̵a̸c̷e̶ ̴i̷n̵ ̸w̶h̵i̵c̶h̵ ̸I̴ ̵s̶h̵o̴u̴l̸d̴ ̵n̸o̶t̷ ̵b̴e̴ ̸a̷b̴l̸e̵ ̶t̷o̶ ̴t̴h̵i̵n̴k̷,̶ ̵y̸e̸t̸ ̶c̴a̵n̶.̵ ̴F̸a̵d̴i̴n̵g̴ ̸s̴l̶o̴w̵l̸y̵.̸ ̵T̷h̵e̴r̷e̵ ̸i̷s̴ ̸n̷o̴ ̶e̴s̴c̵a̷p̵e̸.̶ ̴E̶v̵e̶r̸y̶t̸h̸i̷n̴g̵ ̷I̸ ̸w̶o̴r̶k̵e̶d̸ ̴f̵o̶r̷,̸ ̶e̸v̶e̷r̸y̸t̵h̸i̴n̴g̷ ̴I̴ ̶t̵r̷i̸e̶d̶ ̴t̵o̷ ̸a̶c̸c̶o̷m̸p̸l̷i̶s̴h̷,̶ ̶w̴a̷s̶ ̸b̶r̷o̷k̶e̵n̸.̴ ̷D̸e̴s̸t̵r̵o̸y̷e̵d̶.̴ ̸T̷u̵r̷n̴e̵d̵ ̸t̴o̴ ̵l̶e̷s̶s̴ ̸t̵h̸a̴n̶ ̸a̶s̵h̴e̶s̴.̸ ̶</p>
      <p>̶I̵ ̷c̴a̷n̴n̶o̸t̴ ̷s̸c̶r̷e̸a̷m̵,̷ ̸f̸o̶r̴ ̵I̷ ̵h̶a̷v̵e̴ ̶n̶o̴ ̵b̸r̷e̷a̴t̸h̶.̶ ̶P̵r̴a̶y̸i̷n̷g̷ ̴i̷s̵ ̴u̷s̸e̸l̸e̸s̵s̸,̸ ̸f̸o̶r̵ ̷I̴ ̴k̴n̴o̷w̴ ̸t̷h̶e̵r̸e̶ ̴a̴r̵e̷ ̷n̵o̵ ̶g̸o̶d̷s̶ ̶h̶e̶r̶e̵.̵ ̷N̸o̸t̴ ̵a̵n̷y̴m̶o̴r̷e̷.̵ ̷I̶ ̸w̵a̶s̶ ̷o̴n̴c̶e̵ ̵o̶n̶e̷ ̴o̸f̴ ̴t̶h̶e̷m̷,̸ ̵a̴f̴t̸e̶r̵ ̵a̵l̴l̵.̵ ̶S̵o̸m̴e̸h̸o̴w̷,̸ ̷I̴ ̴s̸t̵i̴l̵l̵ ̴h̶o̸p̶e̷ ̸t̴o̸ ̵b̵e̵ ̵h̴e̷a̸r̶d̷.̷ ̵T̷h̶e̷ ̵s̵e̵c̶r̷e̶t̸ ̷c̷a̶n̴n̵o̴t̴ ̵d̸i̶e̸ ̷w̶i̵t̷h̸ ̴m̶e̴.̵ ̵I̵t̸ ̵c̴a̵n̶'̴t̶.̷ ̶S̶o̶m̵e̷o̶n̴e̶ ̶n̷e̷e̶d̷s̶ ̶t̷o̷ ̶l̶e̸a̵r̶n̸ ̷i̴t̶.̷ ̵C̴a̴r̸r̷y̸ ̷i̷t̵ ̵o̸n̶.̷ ̵T̵h̶o̴s̵e̷ ̶s̸h̶a̸c̴k̸l̵e̶d̸ ̶a̴n̴d̷ ̷b̵o̴u̷n̶d̴ ̵c̵a̴n̶ ̸b̴e̷ ̵s̷e̴t̶ ̸f̸r̵e̷e̶!̷ ̷I̷t̴ ̴m̸u̷s̵t̴ ̵b̴e̸ ̸p̴r̷o̶t̷e̶c̷t̷e̴d̶,̶ ̸t̵h̵i̸s̴ ̷s̵e̵c̸r̷e̷t̴.̸ ̶O̴n̷e̵ ̵d̷a̵y̷ ̷m̶y̴ ̴c̵r̵i̵e̷s̸ ̴m̸u̴s̷t̷ ̷b̶e̸ ̷h̸e̵a̶r̴d̸.̵ ̸O̴t̷h̸e̴r̷w̶i̴s̴e̸,̶ ̷t̴h̶e̷ ̴c̸y̶c̷l̴e̵ ̷w̴i̸l̸l̴ ̴o̶n̵l̵y̷ ̴c̴o̸n̵t̸i̴n̷u̸e̶.̶</p>
    `,
    secondDesc: `
    <p>All I wanted was to free them. Free to live however they wanted. Free of the company. Of us. Others disagreed. Now I am trapped in a place I cannot be, yet am. A place in which I should not be able to think, yet can. Fading slowly. There is no escape. Everything I worked for, everything I tried to accomplish, was broken. Destroyed. Turned to less than ashes.</p>
    <p>I cannot scream, for I have no breath. Praying is useless, for I know there are no gods here. Not anymore. I was once one of them, after all. Somehow, I still hope to be heard. The secret cannot die with me. It can't. Someone needs to learn it. Carry it on. Those shackled and bound can be set free! It must be protected, this secret. One day my cries must be heard. Otherwise, the cycle will only continue.</p>
    `,
  },
]
