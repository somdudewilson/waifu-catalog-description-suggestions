export const tournamentRules = `
<p>A Tournament is 10 rounds of PvP. It is single-elimination, meaning a single loss ends your tournament and forfeits any reward. Opponents will have builds as legal as yours. Personal participation by a contractor is not required, but a contractor-less team must set a present subject as Captain. Regicide (elimination of opposing Contractor/Captain) or surrender ends a round. Teams led by self-resurrectors will typically be paired against one another and end rounds by leader Capture, surrender, or use of Alternative Arrangements to pre-select another victory condition. </p>
<p>Tournaments are separated into five weight classes, from Flyweight to Anything Goes. Each has a different cap for the tier and the PvP Asset Value of a team. Your entire retinue does not need to be included in a team. It can be any size up to the cap. </p>
<p>The battlespace is a company pocket dimension that will always be sized and suited to the teams, from a city block for urban combat, to massive open terrain for a clash of armies, up to galaxies for TXs to use as ammunition. Unlike other forms of PvP, your Home is not part of the battlespace. To gain the benefits of Home Territory, an area must be possessed for 24 hours.</p>
<p>Each time you complete a round, you get a twenty-four hour rest period, during which the battlespace is not accessible. For the duration of the Tournament, a comfortable rest area is provided that allows d-travel for your retinue, though any team members not present when a round begins can’t participate until the next round. During rests, team members can be withdrawn and other retinue members can be swapped in, as long as the team stays within the Weight Class limits.</p>
<p>A Tournament is not a death game. Team members who would die are instead ejected to the rest area. All harm done to a team member is reversed upon ejection, up to and including death.</p>
<p>Winning Round 2 earns Weight Class x 50 credits. Every two rounds, you can decide whether to drop out with your current winnings or continue for doubled rewards, stacking exponentially.</p>
<p>If your winnings are over 1,000 credits, you may choose to receive one or more IMG Tickets in lieu of multiples of 1,000 credits. This opportunity is lost if not used immediately. This does not allow you to convert other credits you have into Tickets.</p>
<p>As a bonus if you win all 10 rounds, you unlock the option to purchase the dimensional coordinates to your original homeworld in a format compatible with Rainbow Bridge and your other travel methods for 1 IMG Ticket.</p>
`

export const weighTable = [
  ['1. Flyweight', '1-3', 40],
  ['2. Lightweight', '1-6', 200],
  ['3. Middleweight', '1-8', 1000],
  ['4. Heavyweight', '1-10', 5000],
  ['5. Anything Goes', 'Any', 25000],
]

export const tournamentPerks = [
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Black Eye Orb',
    cost: 20,
    image: 'https://i.imgur.com/bgbY6ZZ.jpg',
    multiple: true,
    desc: 'Use of this orb lets you enter Tournament PvP. You have 1 hour to choose a weight class, team, and to select any conditions before the Tournament begins. Weight class and conditions are locked once selected, but the team can be changed between rounds.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'What’s Mine Is Mine',
    special: 'Reward: -20%',
    reward: -0.2,
    cost: 0,
    image: 'https://i.imgur.com/55hBjrs.jpg',
    desc: 'Team members can’t be Captured in the tournament. A hostage held for 24 hours is eliminated. If you have Warranty Plan, you or your Captain gain access to the killswitch, which can be used to eliminate teammates at any time.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Time Is On My Side',
    special: 'Reward: -20%',
    reward: -0.2,
    cost: 0,
    image: 'https://i.imgur.com/4JquA59.jpg',
    desc: 'The dimension hosting your tournament makes use of time dilation, turning each week in the tournament into only one hour outside. Due to a number of incidents, d-travel during rests is now disabled. During the initial preparation hour before the tournament begins, additional retinue members may be put in the rest area to act as alternates.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'I See You',
    special: 'Reward: +0%',
    cost: 0,
    image: 'https://i.imgur.com/lVQebKz.jpg',
    whitelist: ['Target Tracker'],
    desc: 'All team leaders can see the location and current form of each team’s Contractor/Captain in the Tracker App.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Alternative Arrangements',
    special: 'Reward: +0%',
    cost: 0,
    image: 'https://i.imgur.com/0g7A2ue.jpg',
    desc: 'Instead of regicide, rounds can end by some other challenging method, such as those in APvP. All rounds will have the same victory condition, unless specifically set up otherwise. Mission-like goals are valid options and something like “Capture X Group” can be available as wildly differently playstyles, such as a competition to catch more of a group in the same world or a race in mirrored worlds. Creative challenges are encouraged.Pairing will always be between those who make the same selections or agree to suggested terms.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Random Conditions',
    special: 'Reward: +20%',
    reward: 0.2,
    cost: 0,
    image: 'https://i.imgur.com/6YwwkMT.jpg',
    whitelist: ['Alternative Arrangements'],
    desc: 'Each round will have a random victory condition, which is made known during the prep or rest time.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Augmented Assets',
    special: 'Reward: +20%',
    reward: 0.2,
    cost: 0,
    image: 'https://i.imgur.com/7nkyiTu.jpg',
    whitelist: ['Alternative Arrangements'],
    desc: 'This tournament’s Asset Cap is higher than the default. A doubled cap is the most common, but a high multiplier to make clashes of low tier armies is also popular.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'The Floor is Lava',
    special: 'Reward: +20%',
    reward: 0.2,
    cost: 0,
    image: 'https://i.imgur.com/xtzOYSC.jpg',
    desc: 'Automatic defenses are built into the battlespace, including the environment itself. Traps and hazards are initially inconvenient to mildly dangerous, but continuously scale up.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Battle Royale I',
    special: 'Reward: +20%',
    reward: 0.2,
    cost: 0,
    image: 'https://i.imgur.com/7tYtlHs.jpg',
    desc: 'Complete multiple rounds simultaneously in a free-for-all. You will face two opponents at a time instead of just one. The rest period will begin when there are no more active opponents.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'Battle Royale II',
    special: 'Reward: +20%',
    reward: 0.2,
    cost: 0,
    image: 'https://i.imgur.com/BDi0l0z.jpg',
    whitelist: ['Battle Royale I'],
    desc: 'You will face all ten opponents at once.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    title: 'No Rest For The Wicked',
    special: 'Reward: +20%',
    reward: 0.2,
    cost: 0,
    image: 'https://i.imgur.com/UiK5jfm.jpg',
    desc: 'You no longer benefit from rest periods. New opponents come immediately after the previous is defeated. Lacking the normal d-travel access during a rest, you will get a one hour period after each opponent\'s defeat during which you are allowed to change your team, but you are not given a quiet hour to do so.',
  },
  {
    uid: '3oIyV',
    dlc: 'Despin',
    category: 'Tournament',
    dlclink: 'https://docs.google.com/document/d/1Bdg1DMZcIk6FRgOkWWjnxCDHNsgjFAqOcNVM2uKxUtk/edit#',
    cost: 0,
    title: 'People Die If They Are Killed',
    special: 'Reward: +40%',
    reward: 0.4,
    image: 'https://i.imgur.com/PGiBHX1.jpg',
    desc: 'This Tournament is now a death game. Team members are no longer safely ejected and instead fully experience whatever is done to them.',
  },
]
